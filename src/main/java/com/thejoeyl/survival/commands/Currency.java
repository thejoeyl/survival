package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Currency implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;
        FileConfiguration config = main.getConfig();

        if (strings.length == 0) {
            player.sendMessage("You have to specify a amount");
            return true;
        } else {
            int coinValue = Integer.parseInt(strings[0]);
            player.sendMessage("Your coinvalue = " + coinValue);
            main.setBalance(player,coinValue);
        }
        return true;
    }
}
