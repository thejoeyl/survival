package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Claim implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;
        int teamNumber = main.teamGet(player);
        if(teamNumber == -1) {
            player.sendMessage(ChatColor.RED + "You can only claim a chunk when you are in a team");
        } else {
            Chunk chunk = player.getWorld().getChunkAt(player.getLocation());
            int chunkFree = main.chunkCheck(chunk);
            if(chunkFree == -1) {
                main.chunkSet(teamNumber,chunk);
                player.sendMessage(ChatColor.GREEN + "You have succesfully claimed this chunk");
            } else {
                player.sendMessage(ChatColor.RED + "This chunk has already been claimed");
            }
        }
        return true;
    }
}
