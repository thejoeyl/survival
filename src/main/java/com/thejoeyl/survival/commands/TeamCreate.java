package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamCreate implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;
        int teamNumber = main.teamGet(player);
        if (teamNumber == -1) {
            int teamNumberNew = main.teamMapSize();
            main.teamAdd(player, teamNumberNew);
            player.sendMessage(ChatColor.GREEN + "Your team has been created");
        } else {
            player.sendMessage(ChatColor.RED + "You are already in a team");
        }
        return true;
    }
}
