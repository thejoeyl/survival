package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamAdd implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (strings.length == 0) {
            sender.sendMessage(ChatColor.RED + "Please enter a name");
            return true;
        }

        Player player = (Player) sender;
        Player target = Bukkit.getServer().getPlayer(strings[0]);

        int teamNumberPlayer = main.teamGet(player);

        if (target == null) {
            player.sendMessage(ChatColor.RED + "Please enter a valid player");
            return true;
        } else {
            int teamNumberTarget = main.teamGet(target);
            if (teamNumberPlayer == -1) {
                player.sendMessage(ChatColor.RED + "You do not have a team");
            } else if (teamNumberTarget == -1) {
                target.sendMessage(ChatColor.GREEN + "You have joined the team of " + player);
                player.sendMessage(ChatColor.GREEN + "The player : " + target + " has joined your team");
                int teamNumber = main.teamGet(player);
                main.teamAdd(target,teamNumber);
            } else {
                player.sendMessage(ChatColor.GREEN + "This player is already in a team");
            }
        }
        return true;
    }
}
