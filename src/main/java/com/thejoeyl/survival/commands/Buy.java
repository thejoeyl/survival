package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Buy implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;

        FileConfiguration config = main.getConfig();

        int amount = main.getConfig().getInt("shop.buy.1.amount");

        ItemStack stone = new ItemStack(Material.STONE, amount);

        int price = main.getConfig().getInt("shop.buy.1.price");

        main.balanceCheck(player);

        Integer balance = main.balanceGet(player);
        if(balance >= price) {
            player.getInventory().addItem(stone);
            player.updateInventory();
            int newBalance = balance - price;
            main.setBalance(player, newBalance);
            player.sendMessage(ChatColor.GREEN + "Transaction complete, you now have " + newBalance + " money left");
        } else {
            player.sendMessage(ChatColor.RED + "You do not have enough money");
        }
        return true;
    }
}
