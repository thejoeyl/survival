package com.thejoeyl.survival.commands;

import com.thejoeyl.survival.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Sell implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;

        FileConfiguration config = main.getConfig();

        int amount = main.getConfig().getInt("shop.sell.1.amount");

        ItemStack cobbelstone = new ItemStack(Material.COBBLESTONE, amount);

        int reward = main.getConfig().getInt("shop.sell.1.reward");

        main.balanceCheck(player);

        int balance = main.balanceGet(player);
        if (player.getInventory().containsAtLeast(new ItemStack(Material.COBBLESTONE), amount)) {
            int newBalance = balance + reward;
            main.setBalance(player,newBalance);
            player.getInventory().removeItem(cobbelstone);
            player.sendMessage(ChatColor.GREEN + "Your new balance is " + newBalance);
        } else {
            player.sendMessage(ChatColor.RED + "You do not have enough cobblestone you need " + amount);
        }
        return true;
    }
}
