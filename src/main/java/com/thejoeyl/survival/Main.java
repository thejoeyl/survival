package com.thejoeyl.survival;

import com.thejoeyl.survival.commands.*;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin {
    private static Main instance;

    public void onEnable() {
        instance = this;
        System.out.println("Survival has been enabled");

        getConfig().options().copyDefaults(true);
        saveConfig();

        this.getCommand("claim").setExecutor(new Claim());
        this.getCommand("createteam").setExecutor(new TeamCreate());
        this.getCommand("teamadd").setExecutor(new TeamAdd());
        this.getCommand("setcurrency").setExecutor(new Currency());
        this.getCommand("buy").setExecutor(new Buy());
        this.getCommand("sell").setExecutor(new Sell());
    }

    private final Map<Player, Integer> team = new HashMap<>();
    private final Map<Integer, Chunk> claimed = new HashMap<>();
    private final Map<Chunk, Integer> claimed2 = new HashMap<>();
    private final Map<Player, Integer> balance = new HashMap<>();

    public static Main getInstance() {
        return instance;
    }

    public void teamAdd(Player player, int teamNumber) {
        team.put(player,teamNumber);
    }

    public int teamGet(Player player) {
        if(team.get(player) == null) {
            player.sendMessage(ChatColor.RED + "You or someone else does not have a team");
            return -1;
        } else {
            int teamNumber = team.get(player);
            return teamNumber;
        }
    }

    public int balanceGet(Player player) {
        int balance1 = balance.get(player);
        return balance1;
    }

    public void setBalance(Player player, int newBalance) {
        balance.put(player,newBalance);
    }

    public void balanceCheck(Player player) {
        if(balance.get(player) == null) {
            balance.put(player, 0);
        }
    }

    public int teamMapSize() {
        int teamNumber = team.size();
        return teamNumber;
    }

    public int chunkCheck(Chunk chunk) {
        if(claimed2.get(chunk) == null) {
            claimed2.put(chunk, -1);
            return -1;
        } else {
            return -2;
        }
    }

    public void chunkSet(int teamNumber, Chunk chunk) {
        claimed.put(teamNumber,chunk);
        claimed2.put(chunk,teamNumber);
    }

    public void onDisable() {
        System.out.println("Survival has been disabled");
    }
}
